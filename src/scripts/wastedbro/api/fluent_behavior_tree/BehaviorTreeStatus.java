package scripts.wastedbro.api.fluent_behavior_tree;

public enum BehaviorTreeStatus
{
    SUCCESS,
    FAILURE,
    RUNNING,
    KILL
}
