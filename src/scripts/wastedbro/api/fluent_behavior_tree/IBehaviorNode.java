package scripts.wastedbro.api.fluent_behavior_tree;

public interface IBehaviorNode
{
    BehaviorTreeStatus tick();
}
