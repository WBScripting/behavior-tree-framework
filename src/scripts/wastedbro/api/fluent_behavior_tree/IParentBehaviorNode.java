package scripts.wastedbro.api.fluent_behavior_tree;

public interface IParentBehaviorNode extends IBehaviorNode
{
    void addChild(IBehaviorNode node);
}
